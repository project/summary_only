<?php

namespace Drupal\summary_only\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\ElementInfo;
use Drupal\Core\Render\ElementInfoManagerInterface;
use Drupal\Core\Security\TrustedCallbackInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'text_summary_only' formatter.
 *
 * @FieldFormatter(
 *   id = "text_summary_only",
 *   label = @Translation("Summary only"),
 *   field_types = {
 *     "text_with_summary"
 *   }
 * )
 */
class TextSummaryOnlyFormatter extends FormatterBase implements TrustedCallbackInterface {
  /**
   * ElementInfo This property stores information about the element.
   *
   * @var mixed
   */
  protected $elementInfo;

  /**
   * MyCustomClass constructor.
   *
   * @param \Drupal\Core\Render\ElementInfo $elementInfo
   *   The element info service.
   */
  public function __construct(ElementInfo $elementInfo) {
    $this->elementInfo = $elementInfo;
  }

  /**
   * The element info manager.
   *
   * @var \Drupal\Core\Render\ElementInfoManagerInterface
   */
  protected $elementInfo;

  /**
   * Constructs a TextSummaryOnlyFormatter object.
   *
   * @param \Drupal\Core\Render\ElementInfoManagerInterface $elementInfo
   *   The element info manager.
   */
  public function __construct(ElementInfoManagerInterface $elementInfo) {
    $this->elementInfo = $elementInfo;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $plugin_id, $plugin_definition, array $context) {
    return new static(
      $container->get('element_info')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'trim_summary' => FALSE,
      'trim_length' => '600',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element['trim_summary'] = [
      '#title' => $this->t('Trim Summary'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('trim_summary'),
    ];
    $element['trim_length'] = [
      '#title' => $this->t('Trimmed limit'),
      '#type' => 'number',
      '#field_suffix' => $this->t('characters'),
      '#default_value' => $this->getSetting('trim_length'),
      '#description' => $this->t('If the summary is not set, the trimmed %label field will end at the last full sentence before this character limit.', ['%label' => $this->fieldDefinition->getLabel()]),
      '#min' => 1,
      '#states' => [
        // Show this field only if the trim_summary is checked above.
        'visible' => [
          ':input[name="fields[body][settings_edit_form][settings][trim_summary]"]' => ['checked' => TRUE],
        ],
        'required' => [
          ':input[name="fields[body][settings_edit_form][settings][trim_summary]"]' => ['checked' => TRUE],
        ],

      ],
    ];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $trim_summary = $this->getSetting('trim_summary') == TRUE ? 'Yes' : 'No';
    $summary[] = $this->t('Trim Summay: @trim_summary', ['@trim_summary' => $trim_summary]);
    if ($this->getSetting('trim_summary') == TRUE) {
      $summary[] = $this->t('Trimmed limit: @trim_length characters', ['@trim_length' => $this->getSetting('trim_length')]);
    }
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    $render_as_summary = function (&$element) {
      // Make sure any default #pre_render callbacks are set on the element,
      // because text_pre_render_summary() must run last.
      $element += $this->elementInfo->getInfo($element['#type']);
      // Add the #pre_render callback that renders the text into a summary.
      $element['#pre_render'][] = [
        TextSummaryOnlyFormatter::class,
        'preRenderSummary',
      ];
      // Pass on the trim length to the #pre_render callback via a property.
      $element['#summary_trim_length'] = $this->getSetting('trim_length');
    };

    // The ProcessedText element already handles cache context & tag bubbling.
    // @see \Drupal\filter\Element\ProcessedText::preRenderText()
    foreach ($items as $delta => $item) {
      $elements[$delta] = [
        '#type' => 'processed_text',
        '#text' => NULL,
        '#format' => $item->format,
        '#langcode' => $item->getLangcode(),
      ];

      if ($this->getSetting('trim_summary') == TRUE && !empty($item->summary)) {
        $elements[$delta]['#text'] = $item->summary;
        $render_as_summary($elements[$delta]);
      }
      else {
        $elements[$delta]['#text'] = $item->summary;
      }
    }

    return $elements;
  }

  /**
   * Pre-render callback.
   *
   * Renders a processed text element's #markup as a summary.
   *
   * @param array $element
   *   A structured array with the following key-value pairs:
   *   - #markup: the filtered text (as filtered by filter_pre_render_text())
   *   - #format: containing the machine name of the filter format to be used to
   *     filter the text. Defaults to the fallback format. See
   *     filter_fallback_format().
   *   - #summary_trim_length: the desired character length of the summary
   *     (used by text_summary())
   *
   * @return array
   *   The passed-in element with the filtered text in '#markup' trimmed.
   *
   * @see filter_pre_render_text()
   * @see text_summary()
   */
  public static function preRenderSummary(array $element) {
    $element['#markup'] = text_summary($element['#markup'], $element['#format'], $element['#summary_trim_length']);
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    return ['preRenderSummary'];
  }

}
