# Summary Only

This module provides a field formatter that helps to render only summary value
of any field having type "text_with_summary".

For a full description of the module, visit the
[project page](https://www.drupal.org/project/summary_only).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/search/summary_only).

## Table of contents

- Installation
- Configuration
- Maintainers

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

Configure the field formatter from display modes.

## Maintainers

Current maintainers:

- [Manish Saharan](https://www.drupal.org/u/manishsaharan29497)

Supporting organizations:

- [Specbee](https://www.drupal.org/specbee) Created this module for you!
